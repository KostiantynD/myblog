from datetime import date
from django.http.response import HttpResponse
from django.shortcuts import render


all_posts = [
    {
         "slug": "hike-in-the-mountains",
         "image": "mountains.jpg",
         "author": "Konstantin", 
         "date": date(2019, 1, 9),
         "title": "Mountain Hiking",
         "exerpt": "There's nothing like the views you get when hiking in the mountains!",
         "content": """ 
         And I wasn't even prepared for what happened whilst I was enjoying the view!""",
         "content": """loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.
         
         loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.

         loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.
         """

    },
     {
         "slug": "python-learning",
         "image": "coding.jpg",
         "author": "Konstantin", 
         "date": date(2022, 1, 9),
         "title": "Python learning",
         "exerpt": "There's nothing like the views you get when hiking in the mountains!",
         "content": """ 
         And I wasn't even prepared for what happened whilst I was enjoying the view!""",
         "content": """loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.
         
         loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.

         loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.
         """

    },
     {
         "slug": "into-the-woods",
         "image": "woods.jpg",
         "author": "Konstantin", 
         "date": date(2021, 1, 9),
         "title": "Forest working",
         "exerpt": "There's nothing like the views you get when hiking in the mountains!",
         "content": """
         And I wasn't even prepared for what happened whilst I was enjoying the view!""",
         "content": """loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.
         
         loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.

         loremLorem ipsum dolor sit amet, consectetur adipiscing elit, sed 
         do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim 
         veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo 
         consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum 
         dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, 
         sunt in culpa qui officia deserunt mollit anim id est laborum.
         """

    }
]

def get_date(posts):
    return posts['date']


# Create your views here.
def starting_page(request):
    sorted_posts = sorted(all_posts, key=get_date)
    latest_posts = sorted_posts[-3:]
    return render(request, "blog/index.html", {
        "posts": latest_posts

    })


def  posts(request):
    return render(request, "blog/all-posts.html", {
        "all_posts": all_posts
    })


def post_detail(request, slug):
    identified_post = next(post for post in all_posts if post['slug'] == slug)
    return render(request, "blog/post-detail.html", {
        "post": identified_post
    })
